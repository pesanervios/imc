package com.example.imcapp.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.imcapp.LoginActivity;
import com.example.imcapp.MainActivity;
import com.example.imcapp.dao.UserDao;
import com.example.imcapp.lib.BCrypt;
import com.example.imcapp.lib.ImcAppDatabase;
import com.example.imcapp.models.User;
import com.example.imcapp.models.UserEntity;
import com.example.imcapp.models.UserMapper;
import java.util.Date;
import android.os.Handler;

public class AuthController {
    private final String KEY_USER_ID = "userId";
    private final String KEY_USER_USERNAME = "userUsername";
    private final String KEY_USER_FIRST_NAME = "userFirstName";
    private final String KEY_USER_LAST_NAME = "userLastName";
    private final String KEY_USER_BIRTHDAY = "userBirthday";
    private final String KEY_USER_HEIGHT = "userHeight";
    private final String KEY_USER_PASSWORD = "userPassword";

    private UserDao userDao;
    private Context ctx;
    private SharedPreferences preferences;

    public AuthController(Context ctx) {
        this.ctx = ctx;
        int PRIVATE_MODE = 0;
        String PREF_NAME = "ImcAppPref";
        this.preferences = ctx.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.userDao = ImcAppDatabase.getInstance(ctx).userDao();
    }

    private void setUserSession(User user) {
    SharedPreferences.Editor editor = this.preferences.edit();
        editor.putLong(KEY_USER_ID,user.getId());
        editor.putString(KEY_USER_USERNAME,user.getUsername());
        editor.putString(KEY_USER_FIRST_NAME,user.getFirstName());
        editor.putString(KEY_USER_LAST_NAME,user.getLastName());
        editor.putString(KEY_USER_HEIGHT, user.getHeightString());
        editor.putString(KEY_USER_PASSWORD,user.getPassword());
        editor.apply();

    }
    public User getUserSession(){
        long id = preferences.getLong(KEY_USER_ID,0);
        String username = preferences.getString(KEY_USER_USERNAME, "");
        String firstName = preferences.getString(KEY_USER_FIRST_NAME,"");
        String lastName = preferences.getString(KEY_USER_LAST_NAME,"");
        String heightString = preferences.getString(KEY_USER_HEIGHT,"");
        String password = preferences.getString(KEY_USER_PASSWORD,"");

        User user = new User(username,firstName, lastName, new Date(),Double.parseDouble(heightString));
        user.setId(id);

        return user;
    }

    public void checkUserSession() {
        long id = preferences.getLong(KEY_USER_ID, 0);

        final int TIMEOUT = 5000;

        new Handler().postDelayed(() -> {
            if (id != 0) {
                Toast.makeText(ctx, "Bienvenido nuevamente", Toast.LENGTH_LONG).show();
                Intent i = new Intent(ctx, MainActivity.class);
                ctx.startActivity(i);
            } else {
                Intent i = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(i);
            }
            ((Activity) ctx).finish();

    }, TIMEOUT);
}

    public void register(User user){
        String hashedPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(hashedPassword);

        UserEntity userEntity = new UserMapper(user).toEntity();

        userDao.insert(userEntity);

        Toast.makeText(ctx, String.format("Usuario %s registrado", user.getUsername()),  Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ctx, LoginActivity.class);
        ctx.startActivity(i);
    }
    public void login(String username, String password){
        UserEntity userEntity = userDao.findbyUsername(username);

        if (userEntity == null){
            Toast.makeText(ctx,"Usuario y/o contraseña Incorrecta", Toast.LENGTH_SHORT).show();
            return;
        }
        User user = new UserMapper(userEntity).toBase();

        if (BCrypt.checkpw(password, user.getPassword())){
            setUserSession(user);
            Toast.makeText(ctx, String.format("Bienvenido %s", username), Toast.LENGTH_SHORT).show();
            Intent i = new Intent(ctx, MainActivity.class);
            ctx.startActivity(i);
            ((Activity) ctx).finish();
        } else {
            Toast.makeText(ctx, "Usuario y/o Contraseña Incorrecta",  Toast.LENGTH_SHORT).show();
        }
    }
    public void logout(){
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.clear();
        editor.apply();
        Toast.makeText(ctx,"Cerrando Sesión", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);
        ((Activity) ctx).finish();
    }
}