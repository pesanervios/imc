package com.example.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.imcapp.controllers.AuthController;
import com.example.imcapp.controllers.EvaluationController;
import com.example.imcapp.models.Evaluation;
import com.example.imcapp.models.User;
import com.example.imcapp.ui.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewEvaluationActivity extends AppCompatActivity {
    private TextInputLayout tilDate, tilWeight;
    private Button btnRegister, btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_evaluation);

        tilDate = findViewById(R.id.activity_new_evaluation_til_date);
        tilWeight = findViewById(R.id.activity_new_evaluation_til_weight);
        btnRegister = findViewById(R.id.activity_new_evaluation_btn_register);
        btnBack = findViewById(R.id.activity_new_evaluation_btn_back);

        tilDate.getEditText().setOnClickListener(view ->{
            DatePickerFragment.showDatePickerDialog(this, tilDate);
        });

        btnRegister.setOnClickListener(view ->{
            String date = tilDate.getEditText().getText().toString();
            String weight = tilWeight.getEditText().getText().toString();

            final String DATE_PATTERN = "yyyy-MM-dd";

            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
            Date dateDate = null;
            try {
                dateDate = dateFormatter.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            double mdWeight = 0.0;

            try {
                mdWeight = Double.parseDouble(weight);
                tilWeight.setError(null);
                tilWeight.setErrorEnabled(false);
            } catch (Exception error) {
                tilWeight.setError("");
            }

            AuthController authController = new AuthController(view.getContext());

            User user = authController.getUserSession();

            Evaluation evaluation = new Evaluation(0, dateDate, mdWeight, user.getId());

            EvaluationController controller = new EvaluationController(view.getContext());

            controller.register(evaluation);

            Toast.makeText(view.getContext(), "Registrar", Toast.LENGTH_SHORT).show();
        });
        btnBack.setOnClickListener(view -> {
            super.onBackPressed();
        });
    }
}