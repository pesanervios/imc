package com.example.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.example.imcapp.controllers.AuthController;
import com.example.imcapp.controllers.EvaluationController;
import com.example.imcapp.models.Evaluation;
import com.example.imcapp.models.User;
import com.example.imcapp.ui.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Date;

public class QueryActivity extends AppCompatActivity {

    private TextView tvDate, tvWeight, tvImc, tvId;
    private Button btnDelete, btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);

        Evaluation evaluation = (Evaluation) getIntent().getSerializableExtra("evaluation");

        tvId = findViewById(R.id.activity_query_tv_id);
        tvDate = findViewById(R.id.activity_query_tv_date);
        tvWeight = findViewById(R.id.activity_query_tv_weight);
        tvImc= findViewById(R.id.activity_query_tv_imc);

        btnDelete = findViewById(R.id.activity_query_btn_delete);
        btnBack = findViewById(R.id.activity_query_btn_back);


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        tvDate.setText(dateFormat.format(evaluation.getDate()));
        tvWeight.setText(Double.toString(evaluation.getWeight()));
        tvImc.setText(Double.toString(evaluation.getWeight()/(1.82 * 1.82)));


        btnDelete.setOnClickListener(view -> {
            EvaluationController controller = new EvaluationController(view.getContext());
            controller.delete(evaluation.getId());
        });

        btnBack.setOnClickListener(view -> {
            super.onBackPressed();
        });
    }
}