package com.example.imcapp.models;

import java.io.Serializable;
import java.util.Date;

public class Evaluation implements Serializable, IEvaluation {

    private long id;
    private Date date;
    private Double weight;
    private long userId;

    public Evaluation(long id, Date date, double weight, long userId) {
        this.id = id;
        this.date = date;
        this.weight = weight;
        this.userId = userId;
    }
    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public Double getWeight() { return weight; }

    @Override
    public long getUserId() { return userId; }
}

