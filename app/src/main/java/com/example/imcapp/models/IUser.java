package com.example.imcapp.models;

import java.util.Date;

public interface IUser {
    long getId();
    String getUsername();
    String getFirstName();
    String getLastName();
    Date getBirthday();
    Double getHeight();
    String getPassword();
    }







