package com.example.imcapp.models;

import java.util.Date;

public interface IEvaluation {
    long getId();
    Date getDate();
    Double getWeight();
    long getUserId();

}
