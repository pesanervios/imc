package com.example.imcapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "users", indices = {@Index(value = "username", unique = true)})
public class UserEntity implements IUser {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "username")
    private String username;

    @ColumnInfo(name = "first_name")
    private String firstName;

    @ColumnInfo(name = "last_name")
    private String lastName;

    @ColumnInfo(name = "birthday")
    private Date birthday;

    @ColumnInfo(name = "height")
    private Double height;

    @ColumnInfo(name = "password")
    private String password;

    public UserEntity(long id, String username, String firstName, String lastName, Date birthday, Double height, String password) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.height = height;
        this.password = password;
    }
    public long getId(){ return id; }
    public String getUsername() { return username; }
    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public Date getBirthday() { return birthday; }
    public Double getHeight() { return height; }
    public String getPassword() { return password; }

    public void setId(long id) { this.id = id; }

    public void setPassword(String password) { this.password = password; }
}





