package com.example.imcapp.models;

import java.util.Date;

public class User implements IUser {
    private long id;
    private String username;
    private String firstName;
    private String lastName;
    private Date birthday;
    private Double height;
    private String password;


    public User(String username, String firstName, String lastName, Date birthday, Double height) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.height = height;
    }


    public long getId(){ return id; }

    public void setId(long id) { this.id = id; }

    public String getUsername() { return username; }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public Date getBirthday() { return birthday; }

    public Double getHeight() { return height; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getHeightString() { return Double.toString(height);}

}
