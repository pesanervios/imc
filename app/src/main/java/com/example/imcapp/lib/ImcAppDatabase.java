package com.example.imcapp.lib;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.example.imcapp.dao.EvaluationDao;
import com.example.imcapp.dao.UserDao;
import com.example.imcapp.models.EvaluationEntity;
import com.example.imcapp.models.UserEntity;
import com.example.imcapp.utils.Converters;

@Database(entities = {UserEntity.class, EvaluationEntity.class}, version = 3)
@TypeConverters({Converters.class})
public abstract class ImcAppDatabase extends RoomDatabase {
      private static  final String DB_NAME = "imc_app_db";
      private static ImcAppDatabase instance;

      public static synchronized ImcAppDatabase getInstance(Context ctx) {
          if (instance == null){
              instance = Room.databaseBuilder(ctx.getApplicationContext(), ImcAppDatabase.class, DB_NAME)
                   .allowMainThreadQueries()
                   .fallbackToDestructiveMigration()
                   .build();
          }
          return instance;
      }
      public abstract UserDao userDao();
      public abstract EvaluationDao evaluationDao();
}
