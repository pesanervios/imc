package com.example.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.AbstractThreadedSyncAdapter;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.imcapp.controllers.AuthController;
import com.example.imcapp.models.User;
import com.example.imcapp.ui.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout tilUsername, tilFirstName, tilLastName, tilBirthday, tilHeight, tilPassword;
    private Button btnRegister, btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tilUsername = findViewById(R.id.activity_register_til_username);
        tilFirstName = findViewById(R.id.activity_register_til_first_name);
        tilLastName = findViewById(R.id.activity_register_til_last_name);
        tilBirthday = findViewById(R.id.activity_register_til_birthday);
        tilHeight = findViewById(R.id.activity_register_til_height);
        tilPassword = findViewById(R.id.activity_register_til_password);
        btnRegister = findViewById(R.id.activity_register_btn_register);
        btnLogin = findViewById(R.id.activity_register_btn_login);


        tilBirthday.getEditText().setOnClickListener(view -> {
                    DatePickerFragment.showDatePickerDialog( this, tilBirthday);
        });

        btnRegister.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), LoginActivity.class);
            startActivity(i);
            finish();
        });

        btnRegister.setOnClickListener(view -> {
            String username = tilUsername.getEditText().getText().toString();
            String firstName = tilFirstName.getEditText().getText().toString();
            String lastName = tilLastName.getEditText().getText().toString();
            String birthday = tilBirthday.getEditText().getText().toString();
            String height = tilHeight.getEditText().getText().toString();
            String password = tilPassword.getEditText().getText().toString();

            final String DATE_PATTERN = "yyyy-MM-dd";

            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
            Date birthdayDate = null;
            try {
               birthdayDate = dateFormatter.parse(birthday);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            double medHeight = 0.00;

            try {
                medHeight = Double.parseDouble(height);
                tilHeight.setError(null);
                tilHeight.setErrorEnabled(false);
            } catch (Exception error) {
                tilHeight.setError("Estatura Incorrecta");
            }

            boolean usernameValid = !username.isEmpty();
            boolean firstNameValid = !firstName.isEmpty();
            boolean lastNameValid = !lastName.isEmpty();
            boolean birthdayValid = !birthday.isEmpty();
            boolean heightValid = !height.isEmpty();
            boolean passwordValid = !password.isEmpty();

            if (!usernameValid) {
                tilUsername.setError("El usuario es invalido");
            } else {
                tilUsername.setError(null);
            }

            if (!firstNameValid) {
                tilFirstName.setError("Campo requerido");
            } else {
                tilFirstName.setError(null);
            }
            if (!lastNameValid) {
                tilLastName.setError("Campo requerido");
            } else {
                tilLastName.setError(null);
            }
            if (!birthdayValid) {
                tilBirthday.setError("Campo requerido");
            } else {
                tilBirthday.setError(null);
            }
            if (!heightValid) {
                tilHeight.setError("Campo requerido");
            } else {
                tilHeight.setError(null);
            }

            if (!passwordValid) {
                tilPassword.setError("Campo requerido");
            } else {
                tilPassword.setError(null);
            }


            User user = new User(username, firstName, lastName, birthdayDate, medHeight);
            user.setPassword(password);

            AuthController controller = new AuthController(view.getContext());

            controller.register(user);

        });

        btnLogin.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), LoginActivity.class);
            startActivity(i);
            finish();
        });

    }
}