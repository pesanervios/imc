package com.example.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.Button;
import android.widget.Toast;

import com.example.imcapp.controllers.AuthController;
import com.example.imcapp.models.User;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogin, btnRegister;
    private TextInputLayout tilUsername, tilPassword;
    private AuthController authController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        authController = new AuthController(this);

        btnLogin = findViewById(R.id.activity_login_btn_login);
        btnRegister = findViewById(R.id.activity_login_btn_register);
        tilUsername = findViewById(R.id.activity_login_til_username);
        tilPassword = findViewById(R.id.activity_login_til_password);

        btnLogin.setOnClickListener(view -> {
            Toast.makeText(view.getContext(), "Iniciando sesión", Toast.LENGTH_SHORT).show();

            String username = tilUsername.getEditText().getText().toString();
            String password = tilPassword.getEditText().getText().toString();

            boolean usernameValid = !username.isEmpty();
            boolean passwordValid = !password.isEmpty();

            if (!usernameValid) {
                tilUsername.setError("El usuario es invalido");
            } else {
                tilUsername.setError(null);
                tilUsername.setErrorEnabled(false);
            }

            if (!passwordValid) {
                tilPassword.setError("Campo requerido");
            } else {
                tilPassword.setError(null);
                tilPassword.setErrorEnabled(false);
            }

            if (usernameValid && passwordValid) {
                authController.login(username, password);
            } else {
                Toast.makeText(view.getContext(), String.format("Usuario y/o Contraseña  Incorrecta", username), Toast.LENGTH_SHORT).show();
            }

        });

        btnRegister.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), RegisterActivity.class);
            startActivity(i);
            finish();
        });

    }
}