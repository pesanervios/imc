package com.example.imcapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.imcapp.models.UserEntity;

@Dao
public interface UserDao {
    @Query("SELECT id, username, first_name, last_name, birthday, height, password FROM users WHERE username = :username LIMIT 1")
    UserEntity findbyUsername (String username);

    @Insert
    long insert(UserEntity user);
}
