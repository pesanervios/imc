package com.example.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.imcapp.controllers.AuthController;
import com.example.imcapp.controllers.EvaluationController;
import com.example.imcapp.models.Evaluation;
import com.example.imcapp.models.User;
import com.example.imcapp.ui.DatePickerFragment;
import com.example.imcapp.ui.EvaluationAdapter;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String DATE_PATTERN = "yyyy-MM-dd";
    private TextView  tvTitle, tvClearFilter;
    private ListView lvEvaluation;
    private TextInputLayout tilFrom, tilTo;
    private Button  btnLogout, btnNewEvaluation, btnFilter;
    private AuthController authController;
    private EvaluationController evaluationController;

    private List<Evaluation> evaluationList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        authController = new AuthController(this);
        evaluationController = new EvaluationController(this);

        User user = authController.getUserSession();

        tvTitle = findViewById(R.id.activity_main_tv_title);
        tvClearFilter = findViewById(R.id.activity_main_tv_clear_filter);
        lvEvaluation = findViewById(R.id.activity_main_lv_evaluation);
        tilFrom = findViewById(R.id.activity_main_field_from);
        tilTo = findViewById(R.id.activity_main_field_to);
        btnLogout = findViewById(R.id.activity_main_btn_logout);
        btnNewEvaluation = findViewById(R.id.activity_main_btn_new_evaluation);
        btnFilter = findViewById(R.id.activity_main_btn_filter);

        tilFrom.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilFrom);
        });

        tilTo.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilTo);
        });

        for (int x = 0; x < 10; ++x) {
            Evaluation newEvaluation = new Evaluation(x, new Date(), 75, user.getId());
            newEvaluation.setId(x);
            evaluationList.add(newEvaluation);
        }

        List<Evaluation> evaluationList = evaluationController.getAll();
        EvaluationAdapter adapter = new EvaluationAdapter(this, evaluationList);

        lvEvaluation.setAdapter(adapter);

        lvEvaluation.setOnItemClickListener(((adapterView, view, index, id) -> {
            Evaluation evaluation = evaluationList.get(index);

            Intent i = new Intent(view.getContext(), QueryActivity.class);
            i.putExtra("evaluation", evaluation);
            view.getContext().startActivity(i);

        }));

        tilFrom.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilFrom);
        });

        tilTo.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilTo);
        });

        btnNewEvaluation.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), NewEvaluationActivity.class);
            view.getContext().startActivity(i);
        });

        btnLogout.setOnClickListener(view -> {
            authController.logout();
        });
        btnFilter.setOnClickListener(view -> {
            String fromStr = tilFrom.getEditText().getText().toString();
            String toStr = tilTo.getEditText().getText().toString();

            boolean validFrom = !fromStr.isEmpty();
            boolean validTo = !toStr.isEmpty();

            if (validFrom && validTo) {
                SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);

                try {
                    Date from = dateFormatter.parse(fromStr);
                    Date to = dateFormatter.parse(toStr);

                    List<Evaluation> evaluationRangeList = evaluationController.getRange(from, to);
                    EvaluationAdapter rangeAdapter = new EvaluationAdapter(this, evaluationRangeList);

                    lvEvaluation.setAdapter(rangeAdapter);

                    lvEvaluation.setOnItemClickListener(((adapterView, rangeView, index, id) -> {
                        Evaluation evaluation = evaluationRangeList.get(index);

                        Intent i = new Intent(rangeView.getContext(), QueryActivity.class);
                        i.putExtra("evaluation", evaluation);
                        rangeView.getContext().startActivity(i);
                    }));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        tvClearFilter.setOnClickListener(view -> {
            tilFrom.getEditText().setText("");
            tilTo.getEditText().setText("");
            lvEvaluation.setAdapter(adapter);
        });
    }
    @Override
    protected void onResume(){
        super.onResume();
        List<Evaluation> evaluationList = evaluationController.getAll();
        EvaluationAdapter adapter = new EvaluationAdapter(this, evaluationList);

        lvEvaluation.setAdapter(adapter);

        lvEvaluation.setOnItemClickListener(((adapterView, view, index, id) -> {
            Evaluation evaluation = evaluationList.get(index);

            Intent i = new Intent(view.getContext(), QueryActivity.class);
            i.putExtra("evaluation", evaluation);
            view.getContext().startActivity(i);
        }));
    }
}