package com.example.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.imcapp.ui.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

public class EvaluationsActivity extends AppCompatActivity {

    private TextInputLayout tilDate4;
    private Button btnEvaluations, btnMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluations);

        tilDate4 = findViewById(R.id.activity_evaluations_til_date4);
        btnEvaluations = findViewById(R.id.activity_evaluations_btn_evaluations);
        btnMain = findViewById(R.id.activity_evaluations_btn_main);

        tilDate4.getEditText().setOnClickListener(view -> {
                    DatePickerFragment.showDatePickerDialog(this, tilDate4);
        });

        btnEvaluations.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), MainActivity.class);
            startActivity(i);
            finish();
        });

        btnMain.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), MainActivity.class);
            startActivity(i);
            finish();
        });
    }

}